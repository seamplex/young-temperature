# GiTeX

A high-yet-still-pretty-low-level layer for document preparation and management: Git + (Xe)LaTeX + (+ Bash \& M4 + optionally Markdown)

## Dependencies

Make sure you have the following tools installed.

### Required

 * Git
 * XeLaTeX
 * Bash
 * M4

```
sudo apt-get install bash m4 sed git texlive-xetex texlive-latex-recommended texlive-fonts-recommended
```

### Optional

 * Biber
 * Inkscape
 * Latexdiff

## Usage


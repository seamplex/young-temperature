% body contents
%  pandoc README.md --no-wrap -t latex | sed s/subsection/section/ | sed s/dnl/\`dnl\'/

\section{Introduction}

This reports shows the results of solving the benchmark problem stated in reference~\cite{hplv100} that consists of a thermo-mechanical problem in which a parallelepiped whose Young’s modulus depends on the temperature is subject to known heat fluxes on its surface and to an external traction pressure at the same time. The geometry, the material properties and the boundary conditions are selected such that the problem has analytical solutions for both the temperature and displacement distributions, as computed by solving the heat diffusion and the linear elasticity equations respectively.

The original problem (and even other solutions such as~\cite{simscaleyoung}) uses an structured grid to exploit the simplicity of the problem domain. However interesting this approach is, it fails to provide a satisfactory validation case because it may be the case that the computer code handles correctly structured hexahedra but fails to obtain good results for arbitrary geometries that need unstructured grids. So in order to partially remedy this issue, in this report we solve the simple geometry that gives rise to analytical solutions but using a not-so-fine unstructured grid to discretize the parallelepiped. This way, the results involve the evaluation of non-trivial jacobians and numerical integrations and if we do get the results, the conclusions about the validation of the code for more general cases is more satisfactory than if we employed just simple structured grids.

\subsection{About fino}

The problem is solved with the free (“Free” both as in “free speech” and in “free beer.”) and open source finite-element analysis tool fino---developed by Seamplex---with a Gmsh-generated unstructured grid. More information about the programs, including documentation and downloads can be found at

\begin{figure}[h]
\begin{center}
\subfloat[\url{https://www.seamplex.com/fino}]{\hspace{1cm}\includegraphics[width=3cm]{qr-fino}\hspace{1cm}}
\hspace{1cm}
\subfloat[\url{http://gmsh.info/}]{\hspace{1cm}\includegraphics[width=3cm]{qr-gmsh}\hspace{1cm}}
\end{center}
\end{figure}

It should be noted that, following Seamplex’ principles,\footnote{\url{https://www.seamplex.com/principles.html}} only free and open source software was used in preparing this report.
%  A complete list and detailed description of the software packages actually used can be found in appendix~\ref{sec:software}.

\section{Problem}
\label{sec:problem}

The problem consists of finding the non-dimensional temperature~$T$ and displacements $u$, $v$ and $w$ distributions within a solid parallelepiped of length~$l$ whose base is a square of $h\times h$ and that is subject to heat fluxes and to a traction pressure at the same time. The geometry is shown in figure~\ref{fig:problem}. The non-dimensional Young’s modulus~$E$ of the material depends on the temperature~$T$ (which in turns depends on space) in a know algebraically way, whilst both the Poisson coefficient~$\nu$ and the thermal conductivity~$k$ are uniform and do not depend on the spatial coordinates:

\begin{align*}
E(x,y,z) &= \frac{1000}{800-T(x,y,z)} \\
\nu &= 0.3 \\
k &= 1 \\
\end{align*}

\begin{figure}[h]
\begin{center}
\includegraphics[width=\linewidth]{problem}
\end{center}
\caption{\label{fig:problem}The problem to be solved. Original figure from reference~\cite{hplv100}.}
\end{figure}


The thermal boundary conditions are:

\begin{minipage}{0.4\linewidth}
\begin{itemize}
 \item Temperature at point~A at $(l,0,0)$ is zero \label{temp0}
 \item Heat flux $q^{\prime \prime}$ through $x=l$ is +2
 \item Heat flux $q^{\prime \prime}$ through $x=0$ is -2
 \item Heat flux $q^{\prime \prime}$ through $y=h/2$ is +3
 \item Heat flux $q^{\prime \prime}$ through $y=-h/2$ is -3
 \item Heat flux $q^{\prime \prime}$ through $z=h/2$ is +4
 \item Heat flux $q^{\prime \prime}$ through $z=-h/2$ is -4
\end{itemize}
\end{minipage}%
\begin{minipage}{0.5\linewidth}
\begin{equation*}
\begin{cases} 
T(l,0,0) = 0\\
\left. -k \cdot \frac{\partial T}{\partial n}\right|_{x=l} = +2     \\
\left. -k \cdot \frac{\partial T}{\partial n}\right|_{x=0} = -2     \\
\left. -k \cdot \frac{\partial T}{\partial n}\right|_{y=h/2} = +3   \\
\left. -k \cdot \frac{\partial T}{\partial n}\right|_{y=-h/2} = -3  \\
\left. -k \cdot \frac{\partial T}{\partial n}\right|_{z=h/2} = +4   \\
\left. -k \cdot \frac{\partial T}{\partial n}\right|_{z=-h/2} = -4  \\
\end{cases}
\end{equation*}
\end{minipage}

% \medskip
\vspace{0.5cm plus 0.2cm minus 0.4cm}

The mechanical boundary conditions are:

\begin{minipage}{0.4\linewidth}
\begin{itemize}
 \item Point O at $(0,0,0)$ is fixed
 \item Point B at $(0,h/2,0)$ is restricted to move only in the~$y$ direction
 \item Point C at $(0,0,/h2)$ cannot move in the~$x$ direction
 \item Surfaces~$x=0$ and~$x=l$ are subject to an uniform normal traction equal to one
\end{itemize}
\end{minipage}%
\begin{minipage}{0.5\linewidth}
\begin{equation*}
\begin{cases} 
u(0,0,0) = 0\\
v(0,0,0) = 0\\
w(0,0,0) = 0\\
u(0,h/2,0) = 0\\
w(0,h/2,0) = 0\\
u(0,0,h/2) = 0\\
\left. p\right|_{x=0} = 1\\
\left. p\right|_{x=l} = 1\\
\end{cases}
\end{equation*}
\end{minipage}



\section{Solution}

The case is a thermo-mechanical problem in which the mechanical properties (namely the Young’s modulus~$E$) depends on the temperature~$T$. But the thermal properties do not depend back on the mechanical properties. Under the assumption of linearity in the geometry (i.e. that the displacements due to the mechanical loads are negligible) the problem is not coupled. It can then be solved by first solving the standalone thermal problem and then using the resulting temperature distribution~$T$ to evaluate~$E$. It should be noted that under these conditions, the problem is also linear in the material properties because~$E$ depends explicitly on~$x$, $y$ and~$z$ and does not depend on the unknowns~$u$, $v$ or~$w$.

\subsection{Analytical solution}

The problem stated in section~\ref{sec:problem} has the following analytical solutions for the temperature and displacement distributions under the heat diffusion equation and the linear elasticity formulation:

\begin{align*}
T(x,y,z) &= -2x -3y -4z + 40 \\
u(x,y,z) &= \frac{A}{2} \cdot\left[x^2 + \nu\cdot\left(y^2+z^2\right)\right] + B\cdot xy + C\cdot xz + D\cdot x - \nu\cdot \frac{Ah}{4} \cdot \left(y+z\right) \\
v(x,y,z) &= -\nu\cdot \left[A\cdot x y + \frac{B}{2} \cdot \left(y^2-z^2+\frac{x^2}{\nu}\right) + C\cdot y z + D\cdot y -A\cdot h/4\cdot x - C\cdot h/4\cdot z\right] \\
w(x,y,z) &= -\nu\cdot \left[A\cdot x z + B\cdot yz + C/2\cdot \left(z^2-y^2+\frac{x^2}{\nu}\right) + D\cdot z + \frac{Ch}{4} \cdot y - \frac{Ah}{4} \cdot x\right] \\
\end{align*}
%
where~$A=0.002$, $B=0.003$, $C=0.004$ and~$D=0.76$. The reference results are the temperature at points O and D and the displacements at points A and D (table~\ref{tab:reference}).

\begin{table}[h]
\rowcolors{1}{black!10}{black!0}
\begin{center}
\begin{tabular}{ccS}
\hline
Point & Unknown &  {Reference value} \\
\hline
O & $T$ & +40.0 \\
D & $T$ & -35.0 \\
A & $u$ & +15.6 \\
  & $v$ & -0.57 \\
  & $w$ & -0.77 \\
D & $u$ & +16.3 \\
  & $v$ & -1.785 \\
  & $w$ & -2.0075 \\
\hline
\end{tabular}
\end{center}
\caption{\label{tab:reference}Reference results the original benchmark problem~\cite{hplv100} asks for.}
\end{table}

This problem is purely of mathematical interest because the assumption of the geometrically linearity implied in the linear elastic problem formulation clearly does not hold as the parallelepiped elongates 75\% of its original length in the~$x$ direction. Moreover, this expansion would change the thermal problem conditions as well, rendering the problem mechanically-thermal coupled.

\subsection{Geometry discretization}

The geometry is fairly simple, although the points A, B, C, D and O need to be explicitly defined and embedded into the grid so boundary conditions can be attached to them. The following Gmsh input script creates a suitable second-order grid for both the thermal and the mechanical problem (figure~\ref{fig:mesh}):

\lstinputlisting[style=C, caption=parallelepiped.geo]{run/parallelepiped.geo}

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.75\linewidth]{run/mesh2.png}
\end{center}
\caption{\label{fig:mesh}A suitable tetrahedral second-order unstructured grid for both thermal and mechanical problems generated by Gmsh. The grid has 
\num[group-separator = {,}]{esyscmd(`grep -C1 \$Nodes run/parallelepiped.msh | tail -n1 | tr -d \\n')}
nodes and
\num[group-separator = {,}]{esyscmd(`grep -C1 \$Elements run/parallelepiped.msh | tail -n1 | tr -d \\n')}
elements counting both tetrahedra and triangles for the discretization of the boundary conditions.}
\end{figure}


\subsection{Thermal problem}
\label{sec:thermal}

The first step is to solve the thermal problem. With the grid from the last section, this relatively simple input\footnote{Fino’s (actually wasora’s~\cite{wasoradesc}) motto is “Simple problems ought to have simple inputs.”}

\begin{enumerate}
 \item reads the mesh (line~1),
 \item selects the problem to be “thermal” (default is “mechanical” line~2),
 \item defines the material properties, only the thermal conductivity~$k$ in this case (line~5)
 \item defines the boundary conditions (lines 8--16),
 \item solves the problem (line~18),
 \item defines the analytical solution $T(x,y,z)$ (line~20),
 \item writes a VTK file with the result and the absolute error with respect to the analytical solution (line~22),  
 \item writes a MSH file with the computed temperature distribution (which can be read back by fino in the mechanical problem, line~23), and
 \item prints to the standard output the results the original benchmark problem asks for~\cite{hplv100} (lines 25--26).
\end{enumerate}

 
\lstinputlisting[style=fino, caption=thermal.fin]{run/thermal.fin}

Running fino~`'esyscmd(`cat run/version.txt') with \texttt{thermal.fin} as the main input file gives the desired results:

\lstinputlisting[style=bash]{run/thermal.txt}


Using the VTK output, some pretty figures illustrating the thermal results can be built as in figure~\ref{fig:results-thermal}. The errors with respect to the analytical solution of the temperature distribution are given as signed absolute values because giving relative errors as a measure of the degree of accuracy of the code is conceptually wrong because the temperature scale is defined up to an additive constant. Thus, as the zero of the scale can be defined by the value of the only Dirichlet boundary condition the problem has (first bullet in page~\pageref{temp0}), relative errors are ill-defined.

\begin{figure}[p]
\begin{center}
\subfloat[Temperature distribution]{\includegraphics[width=0.95\linewidth]{run/thermal1.png}}

\subfloat[Absolute error]{\includegraphics[width=0.4\linewidth]{run/thermale-clipx.png}}

\subfloat[Absolute error]{\includegraphics[width=0.4\linewidth]{run/thermale-clipy.png}}\hspace{0.1\linewidth}
\subfloat[Absolute error]{\includegraphics[width=0.4\linewidth]{run/thermale-clipz.png}}
\end{center}
\caption{\label{fig:results-thermal}Resulting temperature distribution as solved by fino using \texttt{thermal.fin} and comparisons with respect to the analytical solution.}
\end{figure}


\subsection{Mechanical problem}

Once the temperature distribution is available, the mechanical problem can be solved. In this case, the Young’s modulus~$E$ depends on temperature. The benchmark problem asks for the numerical temperature computed in the last section, although we may use the actual analytical solution also. The latter can be easily defined in fino\footnote{Actually this line~is handled by the wasora framework (\url{https://www.seamplex.com/wasora} on top of which fino is developed~\cite{wasoradesc}.} it has already been done in the thermal problem as

\lstinputlisting[style=fino, caption=real.fin]{run/real.fin}

The former can be read from the numerical output written by \texttt{thermal.fin} in a Gmsh file, altogether with the mesh topology:

\lstinputlisting[style=fino, caption=computed.fin]{run/computed.fin}

In this case, both the thermal and the mechanical problems are solved using the same second-order grid and as such, the temperature~$T$ and the Young’s modulus~$E$ will have the same points of definition and interpolation functions so the performance will be better. However, fino can handle the case where the numerical~$T$ that~$E$ depends on is defined in a different grid, for example with a different number of nodes or with elements with a different order.

The mechanical problem can the be solved with the following fino input, which needs an extra command line argument to use either the analytical temperature distribution (\texttt{real}) or the numerical one (\texttt{computed}), and that

\begin{enumerate}
 \item includes either \texttt{real.fin} or \texttt{computed.fin} to define~$T(x,y,z)$ (line~5),
 \item sets~$E$ as an algebraically-defined function of space\footnote{The original reference~\cite{hplv100} says \begin{french}“Il est nécessaire de prévoir un grand nombre de points de discrétisation de la courbe~$E(T)$ pour obtenir la précision souhaitée. Ici on a pris 250 points~$(E_i, T_i)$.”\end{french} With fino, line~6 is all that it takes. No need to worry about the definition points, it takes care by itself to sample the function wherever it is needed.} (through $T(x,y,z)$) and~$\nu$ as a constant (lines 6--7),
 \item defined the boundary conditions (lines 10--14),
 \item solves the problem (line~16),
 \item defines the analytical solutions (lines 19--27),
 \item writes a VTK file with the three absolute errors and a vector with the computed displacements (line~29),
 \item prints to the standard output the results the original benchmark problem asks for~\cite{hplv100} (lines 31--37).
\end{enumerate}

\lstinputlisting[style=fino, caption=mechanical.fin]{run/mechanical.fin}

In this case, fino ought to be run with \texttt{mechanical.fin} as the main input file and an extra argument, either \texttt{real} or \texttt{computed}:

\lstinputlisting[style=bash]{run/computed.txt}


\lstinputlisting[style=bash]{run/real.txt}

There is no appreciable difference between both cases. And again, pretty pictures like figure~\ref{fig:results-mechanical} can be built out of the post-processing files generated by Fino.

\begin{figure}[p]
\begin{center}
\subfloat[Original and strained geometry. The color range shows the modulus of the displacement vector~$\left|u(x,y,z), v(x,y,z), w(x,y,z)\right|$.]{\includegraphics[width=\linewidth]{run/mechanical1.png}}

\subfloat[Absolute error in~$u(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalu-clipx.png}}
\subfloat[Absolute error in~$u(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalu-clipy.png}}
\subfloat[Absolute error in~$u(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalu-clipz.png}}

\subfloat[Absolute error in~$v(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalv-clipx.png}}
\subfloat[Absolute error in~$v(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalv-clipy.png}}
\subfloat[Absolute error in~$v(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalv-clipz.png}}

\subfloat[Absolute error in~$w(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalw-clipx.png}}
\subfloat[Absolute error in~$w(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalw-clipy.png}}
\subfloat[Absolute error in~$w(x,y,z)$]{\includegraphics[width=0.33\linewidth]{run/mechanicalw-clipz.png}}
\end{center}
\caption{\label{fig:results-mechanical}Results of the mechanical problem with the numerical temperature distribution computed in section~\ref{sec:thermal}}
\end{figure}

\subsection{Coarser and finer grids}

A quick grid sensitivity study can be performed from the commandline:\footnote{To see how to perform full parametric studies using fino’s (actually, wasora’s) functionality see reference~\cite{veeder}.}

\lstinputlisting[style=bash]{run/grids.txt}



\section{Conclusions}

The benchmark problem from reference~\cite{hplv100} was solved using the free and open source solver fino. An unstructured grid of approximate~15k nodes was generated using Gmsh in order to validate the usage of general tetrahedra with arbitrary jacobians and non-trivial transformations from real to canonical coordinate systems. Under the assumption of linear geometry (i.e. that the mechanical displacements do not distort the grid) and linear elasticity then the problem is not coupled in the mechanical-thermal direction and has analytical solutions for the temperature distribution~$T$ and the displacements~$u$, $v$ and~$w$. The numerical solution obtained with fino, first by solving just the thermal problem and then by plugging the obtained temperature distribution into a mechanical problem where the Young’s modulus~$E$ depended on~$T(x,y,z)$ had absolute errors of the order of~\num{e-6} with respect to the analytical solutions. The same second-order grid was used for both problems, although similar results would have been obtained for the thermal problem if just first-order tetrahedra were used.

A quick grid sensitivity analysis shows that errors in the thermal problem are insensitive to the number of elements while in the mechanical problem errors slightly increase (decrease) with coarser (finer) grids. This is expected, as the thermal solution is purely linear so the errors do not come from spatial discretization issues. The mechanical problem is slightly more complex, but second-order elements are able to retain the physics and satisfactorily reproduce the analytical results.

\medskip

It should be noted that this non-dimensional problem has a purely mathematical interest, and can be used to show that the same problem solved analytically on the one hand and numerical on the other hand give equivalent results. Clearly, the assumption that the geometry is linear does not hold as the domain expands almost at twice of its original length in the~$x$ direction. This fact renders the problem both non-linear and fully coupled, because the mechanical deformations change the areas of the surfaces that are subject to heat fluxes, not to mention that a material strained that way will for sure stop obeying Hooke’s law. Nevertheless, the linear problem is a nice test case for computational finite-element codes and the results encourage us to keep on working on fino and its web-based front-end CAEplex.
\begin{figure}[h]
\includegraphics[width=\linewidth]{screenshot6}
\caption{\label{fig:screenshot6}Fino and Gmsh running as a back-ends on the cloud for the web-based front-end CAEplex at \url{caeplex.com}}
\end{figure}



\printbibliography

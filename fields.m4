divert(-1)
define(`gitex_title',            `Parallelepiped whose Young’s modulus\\is a function of the temperature')
define(`gitex_subtitle',         `Fino benchmark problem series')  # optional

#define(`gitex_docnumber',        `SP-GITEX-16-2EA8')
# a nice and nerdy way to number documents is to compute the first four digits of the MD5 sum of the title
define(`gitex_docnumber',        `SP-FI-17-BM-esyscmd(echo gitex_title | md5sum | cut -c-4 | tr -d "\n" | tr "[:lower:]" "[:upper:]")')

define(`gitex_abstract',`
A benchmark thermal-mechanical problem that has an analytical solution was solved using fino. Even though the original problem is defined over a simple parallelepiped, this reports uses a fully unstructured grid to validate the code using arbitrary jacobians and not just structured hexahedra. The way fino handles dependence of parameters on space, in this case through intermediate distributions such as the temperature, is illustrated by the simplicity of the input files used to solve both the thermal and the mechanical problems. The comparison between the numerical and the analytical solutions are within the order of~\num{e-6} even for coarse grids.
')
define(`gitex_doctype',          `Benchmark problem')
define(`gitex_mainlanguage',     `english')
define(`gitex_otherlanguages',   `french')
#define(`gitex_fontsize',         `11pt')

define(`gitex_institution',      `Seamplex')
define(`gitex_keywords',         `Fino`,' benchmark`,' CodeAster`,' thermal conduction`,' linear elasticity')

## if this macro is not defined, the document author is taken as the author of the commit marked as the repository head
define(`gitex_firstauthor',     `Jeremy Theler')
define(`gitex_firstauthoremail',`jeremy@seamplex.com')



#define(`gitex_secondauthor',     `John Doe')
#define(`gitex_secondauthoremail',`jdoe@internet.com')

#define(`gitex_reviewer',         `My boss')
#define(`gitex_revieweremail',    `boss@internet.com')

#define(`gitex_releaser',         `His boss')
#define(`gitex_releaseremail',    `chief@internet.com')

#define(`gitex_client',           `Pinky \& Brain')
#define(`gitex_project',          `Conquer the World')


define(`gitex_includerev',        `')
define(`gitex_includetoc',        `')
#define(`gitex_includelof',        `')

define(`gitex_logo1hor',          `logo1-hor.pdf')

define(`gitex_logo1ver',          `logo1-ver.pdf')
define(`gitex_logo1ver_width',    `25')   # in mm

#define(`gitex_logo2ver',          `logo2-ver.png')
#define(`gitex_logo2ver_width',    `50')   # in mm

#define(`gitex_distribution', `
#Dr. Sheldon Cooper     & CalTech      & scooper@caltech.edu\\
#Dr. Leonard Hofstaeder & CalTech      & lhofstaeder@caltech.edu\\
#Mr. Howard Wolowitz    & CalTech      & hwolowitz@caltech.edu\\
#')
#define(`gitex_cc', `
#Dr. Amy Farah Fowler   &                        & \\
#Mrs. Penny             & Cheese Cake Factory    & \\
#')

define(`gitex_extrapreamble',`
\usepackage{lipsum}
\include{syntax}
')



include(`defaults.m4')
divert(0)dnl

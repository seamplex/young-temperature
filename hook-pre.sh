#!/bin/bash
#
# this script is executed before compiling the latex source
# it receives the current tex file (including the extension)
# as the first argument
cd run
 fino | head -n1 | awk '{print $2}' > version.txt

 if [ ! -e parallelepiped.msh ]; then
  gmsh -v 0 -3 parallelepiped.geo
 fi
#  grep -C1 \$Nodes parallelepiped.msh | tail -n1 | tr -d '\n' > nodes.txt
#  grep -C1 \$Elements parallelepiped.msh | tail -n1 | tr -d '\n' > elements.txt

# #  gmsh figure.geo
#  echo '$ fino thermal.fin' > thermal.txt
#  fino thermal.fin >> thermal.txt
#  echo '$' >> thermal.txt
# 
#  echo '$ fino mechanical.fin computed' > computed.txt;
#  fino mechanical.fin computed >> computed.txt
#  echo '$' >> computed.txt
# 
#  echo '$ fino mechanical.fin real' > real.txt;
#  fino mechanical.fin real >> real.txt
#  echo '$' >> real.txt
cd ..

<http://www.code-aster.org/V2/doc/default/fr/man_v/v7/v7.03.100.pdf>

<https://www.simscale.com/docs/content/validation/ParallelepipedWhoseYoungModulusIsFunctionOfTheTemperature/ParallelepipedWhoseYoungModulusIsFunctionOfTheTemperature.htm>


```
gtheler@tom:~/run/benchmarks/young-temperature$ fino thermal.fin 
T(O) =  40      error = -6.81466e-07
T(D) =  -35     error = -1.62608e-05
gtheler@tom:~/run/benchmarks/young-temperature$ fino mechanical.fin real
u(A) =  15.6    error = -2.55016e-07
v(A) =  -0.569995       error = 4.61804e-06
w(A) =  -0.769991       error = 9.37471e-06
u(D) =  16.3    error = -6.50016e-06
v(D) =  -1.78499        error = 1.45574e-05
w(D) =  -2.0075 error = -4.3095e-06
gtheler@tom:~/run/benchmarks/young-temperature$ fino mechanical.fin computed
u(A) =  15.6    error = -1.10202e-07
v(A) =  -0.569995       error = 5.12521e-06
w(A) =  -0.76999        error = 9.88037e-06
u(D) =  16.3    error = -6.57283e-06
v(D) =  -1.78499        error = 1.49738e-05
w(D) =  -2.0075 error = -3.98694e-06
gtheler@tom:~/run/benchmarks/young-temperature$ 
```

//
Merge "parallelepiped.geo";

General.Trackball = 1;
General.TrackballQuaternion0 = -0.150;
General.TrackballQuaternion1 = 0.30;
General.TrackballQuaternion2 = 0.05;
General.TrackballQuaternion3 = 0.95;
Geometry.PointSize = 8;
Geometry.Points = 1;
Geometry.Color.Points = {0,0,0};
Geometry.Color.Lines = {0,0,128};
General.GraphicsFontSize = 28;
Geometry.LabelType = 2; // Type of entity label (1=elementary number, 2=physical number)
Geometry.PointNumbers = 1;
Mesh.SurfaceEdges = 1;
Mesh.SurfaceFaces = 1;
// Save "mesh.svg";
// Save "mesh.png";

Mesh.Clip = 1;
Mesh.VolumeFaces = 1;
General.Clip0A = -1;
General.Clip0B = 0;
General.Clip0C = 0;
General.Clip0D = 10;
General.ClipWholeElements = 1;

Mesh 3;

Save "mesh2.svg";
Save "mesh2.png";

Exit;

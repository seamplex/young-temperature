echo '$ gmsh -3 -clscale 1.5 parallelepiped.geo | grep vertices | grep elements' > grids.txt
gmsh -3 -clscale 1.5 parallelepiped.geo | grep vertices | grep elements >> grids.txt

echo '$ fino thermal.fin' >> grids.txt
fino thermal.fin >> grids.txt

echo '$ fino mechanical.fin computed' >> grids.txt
fino mechanical.fin computed >> grids.txt

echo '$ fino mechanical.fin real' >> grids.txt
fino mechanical.fin real >> grids.txt


echo '$ gmsh -3 -clscale 0.85 parallelepiped.geo | grep vertices | grep elements' >> grids.txt
gmsh -3 -clscale 0.85 parallelepiped.geo | grep vertices | grep elements >> grids.txt

echo '$ fino thermal.fin' >> grids.txt
fino thermal.fin >> grids.txt

echo '$ fino mechanical.fin computed' >> grids.txt
fino mechanical.fin computed >> grids.txt

echo '$ fino mechanical.fin real' >> grids.txt
fino mechanical.fin real >> grids.txt

echo '$' >> grids.txt

gmsh -v 0 -3 parallelepiped.geo
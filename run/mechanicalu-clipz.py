try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

a1_uxyzurealxyz_PiecewiseFunction = CreatePiecewiseFunction( Points=[-1.13485e-05, 0.0, 0.5, 0.0, 8.4379e-06, 1.0, 0.5, 0.0] )

a3_uvw_PiecewiseFunction = CreatePiecewiseFunction( Points=[0.0, 0.0, 0.5, 0.0, 16.5198730443124, 1.0, 0.5, 0.0] )

a1_vxyzvrealxyz_PiecewiseFunction = CreatePiecewiseFunction( Points=[-6.68138e-06, 0.0, 0.5, 0.0, 7.90145e-06, 1.0, 0.5, 0.0] )

a1_uxyzurealxyz_PiecewiseFunction = CreatePiecewiseFunction( Points=[-1.13485e-05, 0.0, 0.5, 0.0, 8.4379e-06, 1.0, 0.5, 0.0] )

a3_uvw_PiecewiseFunction = CreatePiecewiseFunction( Points=[0.0, 0.0, 0.5, 0.0, 16.5198730443124, 1.0, 0.5, 0.0] )

a1_uxyzurealxyz_PiecewiseFunction = CreatePiecewiseFunction( Points=[-1.13485e-05, 0.0, 0.5, 0.0, 8.4379e-06, 1.0, 0.5, 0.0] )

a3_uvw_PiecewiseFunction = CreatePiecewiseFunction( Points=[0.0, 0.0, 0.5, 0.0, 16.5198730443124, 1.0, 0.5, 0.0] )

a1_uxyzurealxyz_PiecewiseFunction = CreatePiecewiseFunction( Points=[-1.13485e-05, 0.0, 0.5, 0.0, 8.4379e-06, 1.0, 0.5, 0.0] )

a3_uvw_PiecewiseFunction = CreatePiecewiseFunction( Points=[0.0, 0.0, 0.5, 0.0, 16.5198730443124, 1.0, 0.5, 0.0] )

a1_uxyzurealxyz_PVLookupTable = GetLookupTableForArray( "u(x,y,z)-ureal(x,y,z)", 1, RGBPoints=[-1.13485e-05, 0.0, 0.0, 1.0, 8.4379e-06, 1.0, 0.0, 0.0], VectorMode='Magnitude', NanColor=[0.498039, 0.498039, 0.498039], ScalarOpacityFunction=a1_uxyzurealxyz_PiecewiseFunction, ColorSpace='HSV', ScalarRangeInitialized=1.0, LockScalarRange=1 )

a3_uvw_PVLookupTable = GetLookupTableForArray( "u-v-w", 3, NanColor=[0.498039, 0.0, 0.0], RGBPoints=[0.0, 0.0, 0.0, 1.0, 4.1299682610781, 0.0, 1.0, 1.0, 8.25993652215623, 0.0, 1.0, 0.0, 12.3899047832343, 1.0, 1.0, 0.0, 16.5198730443124, 1.0, 0.0, 0.0], ScalarOpacityFunction=a3_uvw_PiecewiseFunction, VectorMode='Magnitude', ScalarRangeInitialized=1.0 )

a1_vxyzvrealxyz_PVLookupTable = GetLookupTableForArray( "v(x,y,z)-vreal(x,y,z)", 1, RGBPoints=[-6.68138e-06, 0.23, 0.299, 0.754, 6.10035e-07, 0.865, 0.865, 0.865, 7.90145e-06, 0.706, 0.016, 0.15], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ScalarOpacityFunction=a1_vxyzvrealxyz_PiecewiseFunction, ColorSpace='Diverging', ScalarRangeInitialized=1.0 )

a1_uxyzurealxyz_PVLookupTable = GetLookupTableForArray( "u(x,y,z)-ureal(x,y,z)", 1, RGBPoints=[-1.13485e-05, 0.0, 0.0, 1.0, 8.4379e-06, 1.0, 0.0, 0.0], VectorMode='Magnitude', NanColor=[0.498039, 0.498039, 0.498039], ScalarOpacityFunction=a1_uxyzurealxyz_PiecewiseFunction, ColorSpace='HSV', ScalarRangeInitialized=1.0, LockScalarRange=1 )

a3_uvw_PVLookupTable = GetLookupTableForArray( "u-v-w", 3, NanColor=[0.498039, 0.0, 0.0], RGBPoints=[0.0, 0.0, 0.0, 1.0, 4.1299682610781, 0.0, 1.0, 1.0, 8.25993652215623, 0.0, 1.0, 0.0, 12.3899047832343, 1.0, 1.0, 0.0, 16.5198730443124, 1.0, 0.0, 0.0], ScalarOpacityFunction=a3_uvw_PiecewiseFunction, VectorMode='Magnitude', ScalarRangeInitialized=1.0 )

a1_uxyzurealxyz_PVLookupTable = GetLookupTableForArray( "u(x,y,z)-ureal(x,y,z)", 1, RGBPoints=[-1.13485e-05, 0.0, 0.0, 1.0, 8.4379e-06, 1.0, 0.0, 0.0], VectorMode='Magnitude', NanColor=[0.498039, 0.498039, 0.498039], ScalarOpacityFunction=a1_uxyzurealxyz_PiecewiseFunction, ColorSpace='HSV', ScalarRangeInitialized=1.0, LockScalarRange=1 )

a3_uvw_PVLookupTable = GetLookupTableForArray( "u-v-w", 3, NanColor=[0.498039, 0.0, 0.0], RGBPoints=[0.0, 0.0, 0.0, 1.0, 4.1299682610781, 0.0, 1.0, 1.0, 8.25993652215623, 0.0, 1.0, 0.0, 12.3899047832343, 1.0, 1.0, 0.0, 16.5198730443124, 1.0, 0.0, 0.0], ScalarOpacityFunction=a3_uvw_PiecewiseFunction, VectorMode='Magnitude', ScalarRangeInitialized=1.0 )

a1_uxyzurealxyz_PVLookupTable = GetLookupTableForArray( "u(x,y,z)-ureal(x,y,z)", 1, RGBPoints=[-1.13485e-05, 0.0, 0.0, 1.0, 8.4379e-06, 1.0, 0.0, 0.0], VectorMode='Magnitude', NanColor=[0.498039, 0.498039, 0.498039], ScalarOpacityFunction=a1_uxyzurealxyz_PiecewiseFunction, ColorSpace='HSV', ScalarRangeInitialized=1.0, LockScalarRange=1 )

a3_uvw_PVLookupTable = GetLookupTableForArray( "u-v-w", 3, NanColor=[0.498039, 0.0, 0.0], RGBPoints=[0.0, 0.0, 0.0, 1.0, 4.1299682610781, 0.0, 1.0, 1.0, 8.25993652215623, 0.0, 1.0, 0.0, 12.3899047832343, 1.0, 1.0, 0.0, 16.5198730443124, 1.0, 0.0, 0.0], ScalarOpacityFunction=a3_uvw_PiecewiseFunction, VectorMode='Magnitude', ScalarRangeInitialized=1.0 )

RenderView1 = CreateRenderView()
RenderView1.CameraViewUp = [-0.17787225246056904, 0.9534933752979232, -0.24333484145838505]
RenderView1.CacheKey = 0.0
RenderView1.StereoType = 0
RenderView1.UseLight = 1
RenderView1.StereoRender = 0
RenderView1.CameraPosition = [37.3894065425965, 11.4093582448954, 25.5935828490992]
RenderView1.LightSwitch = 0
RenderView1.Background2 = [0.0, 0.0, 0.164705882352941]
RenderView1.CameraClippingRange = [15.815068681005698, 68.52301105254517]
RenderView1.StereoCapableWindow = 0
RenderView1.Background = [1.0, 1.0, 1.0]
RenderView1.CameraFocalPoint = [11.8278105174164, -0.29668865726416, -1.59093158789849]
RenderView1.CenterAxesVisibility = 0
RenderView1.CameraParallelScale = 12.2474487139159
RenderView1.CenterOfRotation = [10.0, 0.0, 0.0]

ScalarBarWidgetRepresentation1 = CreateScalarBar( Title='||(u,v,w)||', Visibility=0, LabelFontSize=10, LabelColor=[0.0, 0.0, 0.5000076295109483], LabelFontFamily='Times', LookupTable=a3_uvw_PVLookupTable, TitleFontSize=10, TitleColor=[0.0, 0.0, 0.5000076295109483], TitleFontFamily='Times' )
GetRenderView().Representations.append(ScalarBarWidgetRepresentation1)

ScalarBarWidgetRepresentation1 = CreateScalarBar( Title='u abs error', Enabled=1, LabelFontSize=10, LabelColor=[0.0, 0.0, 0.5000076295109483], LabelFontFamily='Times', LookupTable=a1_uxyzurealxyz_PVLookupTable, TitleFontSize=10, TitleColor=[0.0, 0.0, 0.5000076295109483], TitleFontFamily='Times' )
GetRenderView().Representations.append(ScalarBarWidgetRepresentation1)

mechanicalcomputed_vtk = LegacyVTKReader( guiName="mechanical-computed.vtk", FileNames=['/home/gtheler/run/benchmarks/young-temperature/run/mechanical-computed.vtk'] )

Clip1 = Clip( guiName="Clip1", InsideOut=1, Scalars=['POINTS', 'u(x,y,z)-ureal(x,y,z)'], Value=-1.4553e-06, ClipType="Plane" )
Clip1.ClipType.Origin = [10.0, 0.0, 0.0]

mechanicalcomputed_vtk = LegacyVTKReader( guiName="mechanical-computed.vtk", FileNames=['/home/gtheler/run/benchmarks/young-temperature/run/mechanical-computed.vtk'] )

SetActiveSource(mechanicalcomputed_vtk)
Clip1 = Clip( guiName="Clip1", InsideOut=1, Scalars=['POINTS', 'u(x,y,z)-ureal(x,y,z)'], Value=-1.4553e-06, ClipType="Plane" )
Clip1.ClipType.Origin = [10.0, 0.0, 0.0]
Clip1.ClipType.Normal = [0.0, 1.0, 0.0]

mechanicalcomputed_vtk = LegacyVTKReader( guiName="mechanical-computed.vtk", FileNames=['/home/gtheler/run/benchmarks/young-temperature/run/mechanical-computed.vtk'] )

SetActiveSource(mechanicalcomputed_vtk)
Clip1 = Clip( guiName="Clip1", InsideOut=1, Scalars=['POINTS', 'u(x,y,z)-ureal(x,y,z)'], Value=-1.4553e-06, ClipType="Plane" )
Clip1.ClipType.Origin = [10.0, 0.0, 0.0]
Clip1.ClipType.Normal = [0.0, 0.0, 1.0]

mechanicalcomputed_vtk = LegacyVTKReader( guiName="mechanical-computed.vtk", FileNames=['/home/gtheler/run/benchmarks/young-temperature/run/mechanical-computed.vtk'] )

SetActiveSource(mechanicalcomputed_vtk)
Clip1 = Clip( guiName="Clip1", InsideOut=1, Scalars=['POINTS', 'u(x,y,z)-ureal(x,y,z)'], Value=-1.4553e-06, ClipType="Plane" )
Clip1.ClipType.Origin = [10.0, 0.0, 0.0]
Clip1.ClipType.Normal = [0.0, 0.0, 1.0]

SetActiveSource(mechanicalcomputed_vtk)
DataRepresentation1 = Show()
DataRepresentation1.EdgeColor = [0.0, 0.0, 0.5000076295109483]
DataRepresentation1.SelectionPointFieldDataArrayName = 'u-v-w'
DataRepresentation1.ScalarOpacityFunction = a1_uxyzurealxyz_PiecewiseFunction
DataRepresentation1.ColorArrayName = ('POINT_DATA', 'u(x,y,z)-ureal(x,y,z)')
DataRepresentation1.ScalarOpacityUnitDistance = 1.1934950606375
DataRepresentation1.LookupTable = a1_uxyzurealxyz_PVLookupTable
DataRepresentation1.Representation = 'Outline'
DataRepresentation1.ScaleFactor = 2.0

SetActiveSource(Clip1)
DataRepresentation1 = Show()
DataRepresentation1.EdgeColor = [0.0, 0.0, 0.5000076295109483]
DataRepresentation1.SelectionPointFieldDataArrayName = 'u(x,y,z)-ureal(x,y,z)'
DataRepresentation1.ScalarOpacityFunction = a1_uxyzurealxyz_PiecewiseFunction
DataRepresentation1.ColorArrayName = ('POINT_DATA', 'u(x,y,z)-ureal(x,y,z)')
DataRepresentation1.ScalarOpacityUnitDistance = 0.526934565342239
DataRepresentation1.LookupTable = a1_uxyzurealxyz_PVLookupTable
DataRepresentation1.ScaleFactor = 1.00000000169022

Render()

try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

a1_T_PiecewiseFunction = CreatePiecewiseFunction( Points=[-35.0, 0.0, 0.5, 0.0, 75.0, 1.0, 0.5, 0.0] )

a1_T_PVLookupTable = GetLookupTableForArray( "T", 1, RGBPoints=[-35.0, 0.23, 0.299, 0.754, 20.0, 0.865, 0.865, 0.865, 75.0, 0.706, 0.016, 0.15], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ScalarOpacityFunction=a1_T_PiecewiseFunction, ColorSpace='Diverging', ScalarRangeInitialized=1.0 )

RenderView1 = CreateRenderView()
RenderView1.CameraViewUp = [-0.25531499439199695, 0.8802820735167547, -0.3998971426300239]
RenderView1.CacheKey = 0.0
RenderView1.StereoType = 0
RenderView1.UseLight = 1
RenderView1.StereoRender = 0
RenderView1.CameraPosition = [34.8601017241084, 22.4709040624375, 33.4437227296637]
RenderView1.LightSwitch = 0
RenderView1.Background2 = [0.0, 0.0, 0.16470588235294117]
RenderView1.CameraClippingRange = [24.97318766592746, 75.5653201687625]
RenderView1.StereoCapableWindow = 0
RenderView1.Background = [1.0, 1.0, 1.0]
RenderView1.CameraFocalPoint = [11.2166612792774, 0.0304793693674257, -0.858544659938008]
RenderView1.CenterAxesVisibility = 0
RenderView1.CameraParallelScale = 12.2474487139159
RenderView1.CenterOfRotation = [10.0, 0.0, 0.0]
RenderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]

ScalarBarWidgetRepresentation1 = CreateScalarBar( Title='T', Enabled=1, LabelFontSize=10, LabelColor=[0.0, 0.0, 0.5019607843137255], LabelFontFamily='Times', LookupTable=a1_T_PVLookupTable, TitleFontSize=10, TitleColor=[0.0, 0.0, 0.5019607843137255], TitleFontFamily='Times' )
GetRenderView().Representations.append(ScalarBarWidgetRepresentation1)

thermal_vtk = LegacyVTKReader( guiName="thermal.vtk", FileNames=['/home/gtheler/run/benchmarks/young-temperature/run/thermal.vtk'] )

DataRepresentation1 = Show()
DataRepresentation1.EdgeColor = [0.0, 0.0, 0.5000076295109483]
DataRepresentation1.SelectionPointFieldDataArrayName = 'T'
DataRepresentation1.ScalarOpacityFunction = a1_T_PiecewiseFunction
DataRepresentation1.ColorArrayName = ('POINT_DATA', 'T')
DataRepresentation1.ScalarOpacityUnitDistance = 1.1934950606375
DataRepresentation1.LookupTable = a1_T_PVLookupTable
DataRepresentation1.ScaleFactor = 2.0



GetRenderView().ViewSize = [1200,1000]
Render()

WriteImage("thermal1.png")
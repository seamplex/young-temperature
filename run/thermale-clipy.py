try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

a1_T_PiecewiseFunction = CreatePiecewiseFunction( Points=[-35.0, 0.0, 0.5, 0.0, 75.0, 1.0, 0.5, 0.0] )

a1_TxyzTrealxyz_PiecewiseFunction = CreatePiecewiseFunction( Points=[-4.20494e-05, 0.0, 0.5, 0.0, 4.78921e-05, 1.0, 0.5, 0.0] )

a3_cellNormals_PiecewiseFunction = CreatePiecewiseFunction( Points=[1.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0] )

a1_T_PVLookupTable = GetLookupTableForArray( "T", 1, RGBPoints=[-35.0, 0.23, 0.299, 0.754, 20.0, 0.865, 0.865, 0.865, 75.0, 0.706, 0.016, 0.15], VectorMode='Magnitude', NanColor=[0.0, 0.0, 0.5019607843137255], ScalarOpacityFunction=a1_T_PiecewiseFunction, ColorSpace='Diverging', ScalarRangeInitialized=1.0 )

a1_TxyzTrealxyz_PVLookupTable = GetLookupTableForArray( "T(x,y,z)-Treal(x,y,z)", 1, RGBPoints=[-4.20494e-05, 0.0, 0.0, 1.0, 4.78921e-05, 1.0, 0.0, 0.0], VectorMode='Magnitude', NanColor=[0.498039, 0.498039, 0.498039], ScalarOpacityFunction=a1_TxyzTrealxyz_PiecewiseFunction, ColorSpace='HSV', ScalarRangeInitialized=1.0 )

a3_cellNormals_PVLookupTable = GetLookupTableForArray( "cellNormals", 3, RGBPoints=[1.0, 0.23, 0.299, 0.754, 1.5, 0.865, 0.865, 0.865, 2.0, 0.706, 0.016, 0.15], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ScalarOpacityFunction=a3_cellNormals_PiecewiseFunction, ColorSpace='Diverging', ScalarRangeInitialized=1.0 )

RenderView1 = CreateRenderView()
RenderView1.CameraViewUp = [-0.25531499439199695, 0.8802820735167547, -0.3998971426300239]
RenderView1.CacheKey = 0.0
RenderView1.StereoType = 0
RenderView1.UseLight = 1
RenderView1.StereoRender = 0
RenderView1.CameraPosition = [34.8601017241084, 22.4709040624375, 33.4437227296637]
RenderView1.LightSwitch = 0
RenderView1.Background2 = [0.0, 0.0, 0.16470588235294117]
RenderView1.CameraClippingRange = [24.97318766592746, 75.5653201687625]
RenderView1.StereoCapableWindow = 0
RenderView1.Background = [1.0, 1.0, 1.0]
RenderView1.CameraFocalPoint = [11.2166612792774, 0.0304793693674257, -0.858544659938008]
RenderView1.CenterAxesVisibility = 0
RenderView1.CameraParallelScale = 12.2474487139159
RenderView1.CenterOfRotation = [10.0, 0.0, 0.0]
RenderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]

ScalarBarWidgetRepresentation1 = CreateScalarBar( TitleFontSize=12, Title='T', LookupTable=a1_T_PVLookupTable, Visibility=0, LabelFontSize=12 )
GetRenderView().Representations.append(ScalarBarWidgetRepresentation1)

ScalarBarWidgetRepresentation2 = CreateScalarBar( Title='abs err', Enabled=1, LabelFontSize=10, LabelColor=[0.0, 0.0, 0.5019607843137255], LabelFontFamily='Times', LookupTable=a1_TxyzTrealxyz_PVLookupTable, TitleFontSize=10, TitleColor=[0.0, 0.0, 0.5019607843137255], TitleFontFamily='Times' )
GetRenderView().Representations.append(ScalarBarWidgetRepresentation2)

ScalarBarWidgetRepresentation1 = CreateScalarBar( ComponentTitle='Magnitude', Title='cellNormals', Visibility=0, LabelFontSize=12, LookupTable=a3_cellNormals_PVLookupTable, TitleFontSize=12 )
GetRenderView().Representations.append(ScalarBarWidgetRepresentation1)

thermal_vtk = LegacyVTKReader( guiName="thermal.vtk", FileNames=['/home/gtheler/run/benchmarks/young-temperature/run/thermal.vtk'] )

Clip1 = Clip( guiName="Clip1", InsideOut=1, Scalars=['POINTS', 'T'], Value=20.0, ClipType="Plane" )
Clip1.ClipType.Origin = [10.0, 0.0, 0.0]
Clip1.ClipType.Normal = [0.0, 1.0, 0.0]

SetActiveSource(thermal_vtk)
DataRepresentation1 = Show()
DataRepresentation1.EdgeColor = [0.0, 0.0, 0.5000076295109483]
DataRepresentation1.ColorAttributeType = 'CELL_DATA'
DataRepresentation1.SelectionPointFieldDataArrayName = 'T'
DataRepresentation1.ScalarOpacityFunction = a3_cellNormals_PiecewiseFunction
DataRepresentation1.ScalarOpacityUnitDistance = 1.1934950606375
DataRepresentation1.AmbientColor = [0.0, 0.0, 0.0]
DataRepresentation1.LookupTable = a3_cellNormals_PVLookupTable
DataRepresentation1.Representation = 'Outline'
DataRepresentation1.ScaleFactor = 2.0

SetActiveSource(Clip1)
DataRepresentation2 = Show()
DataRepresentation2.EdgeColor = [0.0, 0.0, 0.5000076295109483]
DataRepresentation2.SelectionPointFieldDataArrayName = 'T'
DataRepresentation2.ScalarOpacityFunction = a1_TxyzTrealxyz_PiecewiseFunction
DataRepresentation2.ColorArrayName = ('POINT_DATA', 'T(x,y,z)-Treal(x,y,z)')
DataRepresentation2.ScalarOpacityUnitDistance = 0.526934565342239
DataRepresentation2.LookupTable = a1_TxyzTrealxyz_PVLookupTable
DataRepresentation2.ScaleFactor = 1.00000000169022


GetRenderView().ViewSize = [1200,1000]
Render()

WriteImage("thermale-clipy.png")
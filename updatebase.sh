if [ -z "$1" ]; then
  git rev-parse HEAD > baserev
else
  hash=`git log -1 $1 | grep ^commit | awk '{print $2}'`
  if [ ! -z "${hash}" ]; then
    echo $hash > baserev
  else
    echo "error: $1 is not a valid commit"
    exit 1
  fi
fi
